function toggleFullScreen() {
  if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.msRequestFullscreen) {
      document.documentElement.msRequestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
}
var sites = ['tv\.sfr\.fr\/television\-sur\-ordinateur\-par\-internet\/'];
var i = 0;
var query="";
while (sites[i]) {
    if (i>0) query+='|'+sites[i];
    else query+=sites[i];
    i++;
}
var thisRegex = new RegExp(query);
var link=window.location.href;
var AZ="OK";
if(thisRegex.test(link)){
    var Int=setTimeout(function () {
        var i;
        x=document.getElementById('btn_recherche_envie');
        if (x!=undefined)x.remove();
            var bouquet=document.getElementsByClassName('tv_bloc_gauche tvpc');
            for (i = 0; i < bouquet.length; i++) {
                bouquet[i].style.zIndex="1000000";
                bouquet[i].style.position="fixed";
                bouquet[i].style.top="0px";
                bouquet[i].style.left="0px";
                bouquet[i].style.backgroundColor="rgba(255,255,255,0.7)";
                bouquet[i].style.opacity="0"
                bouquet[i].style.height="95%";
                bouquet[i].style.textAlign="center";
                bouquet[i].onmouseover = function(){this.style.opacity="1"};
                bouquet[i].onmouseout = function(){this.style.opacity="0"};
                var im=document.createElement("img");
                im.src=chrome.extension.getURL('imgs/fullscreen.png');
                im.style.width="64px";
                im.style.cursor="pointer";
                im.onclick = function(){toggleFullScreen();};
                bouquet[i].appendChild(im);
                var st=document.createElement("style");
                st.type="text/css";
                st.innerHTML="* { transition:opacity 1s linear; }";
                bouquet[i].appendChild(st);
                break;
            }
            var cs=document.getElementsByClassName('channel_selection2');
            for (i = 0; i < cs.length; i++) {
                cs[i].style.backgroundColor="transparent";
                break;
            }
            var ll=document.getElementsByClassName('bouquet');
            for (i = 0; i < ll.length; i++) {
                ll[i].style.backgroundColor="transparent";
                break;
            }
        x = document.getElementsByTagName("object");
        for (i = 0; i < x.length; i++) {
            x[i].style.position="fixed";
            x[i].style.left="0px";
            x[i].style.top="0px";
            x[i].style.width="100%";
            x[i].style.height="100%";
            x[i].style.zIndex="100000";
            chrome.runtime.sendMessage({"message": "signal", "stat": true});
            break;
        }
        if (document.getElementById('assistance')!=undefined) document.getElementById('assistance').remove();
        if (document.body!=undefined) document.body.style.overflow="hidden";
    }, 5000);
}

chrome.runtime.sendMessage({"message": "PID", "stat": true});



